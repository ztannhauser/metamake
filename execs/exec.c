
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <libexplain/fork.h>
#include <libexplain/chdir.h>
#include <libexplain/execvp.h>
#include <libexplain/waitpid.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "exec.h"

int exec(char* args[])
{
	int error = 0;
	int wstatus;
	pid_t child = -1;
	ENTER;
	
	dpvs(args[0]);
	
	if (child = fork(), child < 0)
		fprintf(stderr, "%s\n", explain_fork()),
		error = e_syscall_failed;
	else if (!child)
	{
		if (execvp(args[0], args) < 0)
			fprintf(stderr, "%s\n", explain_execvp(args[0], args)),
			error = e_syscall_failed;
	}
	else if (waitpid(child, &wstatus, 0) < 0)
		fprintf(stderr, "%s\n", explain_waitpid(child, &wstatus, 0)),
		error = e_syscall_failed;
	else if (wstatus)
		fprintf(stderr, "%s: '%s' returned with non-zero exit code!\n", argv0, args[0]),
		error = e_subcommand_failed;
	
	EXIT;
	return error;
}





















