
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <debug.h>
#include <string.h>

#include <libexplain/execvp.h>

#include <error.h>
#include <debug.h>

#include <defines/argv0.h>

#include <misc/module_load.h>

#include <string_list/struct.h>

#include "exec.h"
#include "exec_envml.h"

static void print(struct string_list* modules, char** args, FILE* out)
{
	size_t i, n;
	
	fprintf(out, "$");
	
	if (modules->n)
	{
		fprintf(out, " envml");
		
		for (i = 0, n = modules->n; i < n; i++)
			fprintf(out, " %s", modules->data[i]);
		
		fprintf(out, " --");
	}
	
	for (i = 0; args[i]; i++)
		fprintf(out, " %s", args[i]);
	
	fprintf(out, "\n");
	fflush(out);
}

int exec_envml(
	const char* cwd,
	struct string_list* modules,
	char** args)
{
	int error = 0;
	ENTER;
	
	int wstatus;
	pid_t child = -1;
	
	if (child = fork(), child < 0)
		fprintf(stderr, "%s: fork(): %s\n", argv0, strerror(errno)),
		error = e_syscall_failed;
	else if (!child)
	{
		error = module_load(modules);
		
		if (!error)
		{
			if (cwd && chdir(cwd) < 0)
				fprintf(stderr, "%s: fchdir(): %s\n", argv0, strerror(errno)),
				error = e_syscall_failed;
			else if (execvp(args[0], args) < 0)
				fprintf(stderr, "%s\n", explain_execvp(args[0], args)),
				error = e_syscall_failed;
		}
	}
	else if (waitpid(child, &wstatus, 0) < 0)
		fprintf(stderr, "%s: waitpid(%i): %s\n", argv0, child, strerror(errno)),
		error = e_syscall_failed;
	else if (wstatus)
	{
		fprintf(stderr, "%s: nonzero exit code from command:\n", argv0);
		print(modules, args, stderr);
		error = e_subcommand_failed;
	}
	
	EXIT;
	return error;
}











