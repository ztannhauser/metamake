
#include <stdio.h>
#include <string.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "exec_wpiped_stdin.h"

int exec_wpiped_stdin(
	pid_t* outgoing_pid,
	int stdin_fd,
	char* const* args)
{
	int error = 0;
	pid_t child = -1;
	ENTER;
	
	child = fork();
	
	if (child < 0)
		fprintf(stderr, "%s: fork(): %s\n", argv0, strerror(errno)),
		error = e_syscall_failed;
	else if (child)
		*outgoing_pid = child;
	else if (dup2(stdin_fd, 0) < 0)
		fprintf(stderr, "%s: dup2(): %s\n", argv0, strerror(errno)),
		error = e_syscall_failed;
	else if (execvp(args[0], args) < 0)
		fprintf(stderr, "%s: execvp(\"%s\"): %s\n", argv0, args[0], strerror(errno)),
		error = e_syscall_failed;
	
	EXIT;
	return error;
}

















