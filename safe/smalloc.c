
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <libexplain/malloc.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "smalloc.h"

int smalloc(void** retval, size_t size)
{
	int error = 0;
	void* ptr = NULL;
	ENTER;
	
	dpv(size);
	
	ptr = malloc(size);
	
	if (!ptr)
	{
		fprintf(stderr, "%s\n", explain_malloc(size));
		error = e_out_of_memory;
	}
	else
		*retval = ptr;
	
	EXIT;
	return error;
}

