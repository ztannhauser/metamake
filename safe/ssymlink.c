
#include <unistd.h>
#include <stdio.h>

#include <libexplain/symlink.h>

#include <debug.h>
#include <error.h>

#include "ssymlink.h"

int ssymlink(const char *oldpath, const char *newpath)
{
	int error = 0;
	ENTER;
	
	if (symlink(oldpath, newpath) < 0)
	{
		fprintf(stderr, "%s\n", explain_symlink(oldpath, newpath));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}

