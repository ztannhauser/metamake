
#include <stdio.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <libexplain/pipe2.h>

#include "spipe2.h"

int spipe2(int pipefd[2], int flags)
{
	int error = 0;
	ENTER;
	
	if (pipe2(pipefd, flags) < 0)
	{
		fprintf(stderr, "%s\n", explain_pipe2(pipefd, flags));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}

