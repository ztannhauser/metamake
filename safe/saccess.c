
#include <unistd.h>
#include <stdio.h>

#include <debug.h>
#include <error.h>

#include <libexplain/access.h>

#include "saccess.h"

int saccess(const char *pathname, int mode)
{
	int error = 0;
	ENTER;
	
	if (access(pathname, mode) < 0)
	{
		fprintf(stderr, "%s\n", explain_access(pathname, mode));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}
