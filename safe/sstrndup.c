
#include <stdio.h>

#include <libexplain/strndup.h>

#include <debug.h>
#include <error.h>

#include "sstrndup.h"

int sstrndup(char** outgoing, const char* data, size_t data_size)
{
	int error = 0;
	ENTER;
	
	char *result = strndup(data, data_size);
	if (!result)
	{
		fprintf(stderr, "%s\n", explain_strndup(data, data_size));
		error = e_out_of_memory;
	}
	else
		*outgoing = result;
	
	EXIT;
	return error;
}

