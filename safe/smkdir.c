
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>

#include <error.h>
#include <debug.h>

#include <libexplain/mkdir.h>

#include "smkdir.h"

int smkdir(const char* pathname, mode_t mode)
{
	int error = 0;
	ENTER;
	
	dpvs(pathname);
	dpvo(mode);
	
	if (mkdir(pathname, mode) < 0 && errno != EEXIST)
	{
		fprintf(stderr, "%s\n", explain_mkdir(pathname, mode));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}

