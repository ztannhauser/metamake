
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <libexplain/open.h>

#include <debug.h>
#include <error.h>

#include "sopen.h"

int sopen(int* retval, const char *pathname, int flags, mode_t mode)
{
	int error = 0;
	ENTER;
	
	dpvs(pathname);
	
	int fd = open(pathname, flags, mode);
	
	if (fd < 0)
	{
		fprintf(stderr, "%s\n", explain_open(pathname, flags, mode));
		error = e_syscall_failed;
	}
	else
		*retval = fd;
	
	EXIT;
	return error;
}

