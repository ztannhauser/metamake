
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "sgetenv.h"

int sgetenv(char** outgoing_env, char* envname)
{
	int error = 0;
	ENTER;
	
	dpvs(envname);
	
	char* value = NULL;
	
	value = getenv(envname);
	
	if (!value)
	{
		fprintf(stderr, "%s: getenv(\"%s\") is not defined!\n", argv0, envname);
		error = e_bad_environment;
	}
	
	if (!error)
	{
		dpvs(value);
		
		*outgoing_env = value;
	}
	
	EXIT;
	return error;
}

