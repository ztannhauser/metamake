

#include <libexplain/fopen.h>

#include <error.h>
#include <debug.h>

#include "sfopen.h"

int sfopen(FILE** outgoing, const char *path, const char *mode)
{
	int error = 0;
	ENTER;
	
	FILE *fp = fopen(path, mode);
	
	if (!fp)
	{
		fprintf(stderr, "%s\n", explain_fopen(path, mode));
		error = e_syscall_failed;
	}
	else
		*outgoing = fp;
	
	EXIT;
	return error;
}
