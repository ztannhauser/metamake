
#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <error.h>

#include <libexplain/setenv.h>

#include "ssetenv.h"

int ssetenv(const char *name, const char *value, int overwrite)
{
	int error = 0;
	ENTER;
	
	dpvs(name);
	dpvs(value);
	
	if (setenv(name, value, overwrite) < 0)
	{
		fprintf(stderr, "%s\n", explain_setenv(name, value, overwrite));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}
