
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <error.h>
#include <debug.h>

#include <libexplain/chdir.h>

#include "schdir.h"

int schdir(const char* pathname)
{
	int error = 0;
	ENTER;
	
	if (chdir(pathname) < 0 && errno != EEXIST)
	{
		fprintf(stderr, "%s\n", explain_chdir(pathname));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}

