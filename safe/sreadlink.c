
#include <stdio.h>

#include <libexplain/readlink.h>

#include <debug.h>
#include <error.h>

#include "sreadlink.h"

int sreadlink(const char *pathname, char *data, size_t data_size)
{
	int error = 0;
	ENTER;
	
	ssize_t len;
	
	len = readlink(pathname, data, data_size);
	
	if (len < 0)
	{
		fprintf(stderr, "%s\n", explain_readlink(pathname, data, data_size));
		error = e_syscall_failed;
	}
	else
		data[len] = '\0';
	
	EXIT;
	return error;
}

