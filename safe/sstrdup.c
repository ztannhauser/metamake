
#include <stdio.h>
#include <string.h>

#include <libexplain/strdup.h>

#include <debug.h>
#include <error.h>

#include "sstrdup.h"

int sstrdup(char** outgoing, const char* data)
{
	int error = 0;
	ENTER;
	
	char *result = strdup(data);
	if (!result)
	{
		fprintf(stderr, "%s\n", explain_strdup(data));
		error = e_out_of_memory;
	}
	else
		*outgoing = result;
	
	EXIT;
	return error;
}

