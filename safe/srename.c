
#include <stdio.h>
#include <errno.h>

#include <error.h>
#include <debug.h>

#include <libexplain/rename.h>

#include "srename.h"

int srename(const char *oldpath, const char *newpath)
{
	int error = 0;
	ENTER;
	
	if (rename(oldpath, newpath) < 0 && errno != EEXIST)
	{
		fprintf(stderr, "%s\n", explain_rename(oldpath, newpath));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}

