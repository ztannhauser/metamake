
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

#include <libexplain/stat.h>

#include <debug.h>
#include <error.h>

int sstat(const char* pathname, struct stat* statbuf)
{
	int error = 0;
	ENTER;
	
	if (stat(pathname, statbuf) < 0)
	{
		fprintf(stderr, "%s\n", explain_stat(pathname, statbuf));
		error = e_syscall_failed;
	}
	
	EXIT;
	return error;
}


