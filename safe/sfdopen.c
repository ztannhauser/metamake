
#include <error.h>
#include <debug.h>

#include <libexplain/fdopen.h>

#include "sfdopen.h"

int sfdopen(FILE** retval, int fd, const char *flags)
{
	int error = 0;
	ENTER;
	
	dpv(fd);
	dpvs(flags);
	
	FILE* f = fdopen(fd, flags);
	if (!f)
	{
		fprintf(stderr, "%s\n", explain_fdopen(fd, flags));
		error = e_syscall_failed;
	}
	else
		*retval = f;
	
	EXIT;
	return error;
}

