
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/limits.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>
#include <defines/REPO_URL.h>

#include "defines/which/git.h"

#include <macros/strequal.h>

#include <misc/stream_extract.h>

#include <safe/schdir.h>
#include <safe/smkdir.h>
#include <safe/srename.h>
#include <safe/sreadlink.h>
#include <safe/ssymlink.h>

#include <execs/exec.h>

#include "update.h"

int update(const char* metamake_path, const char* metadata_path)
{
	int error = 0;
	ENTER;
	
	dpvs(metamake_path);
	
	char buf[PATH_MAX];
	
	error = sreadlink(metadata_path, buf, PATH_MAX);
	
	dpvs(buf);
	
	char new_metadata_path[PATH_MAX];
	
	if (!error)
	{
		stpcpy(stpcpy(new_metadata_path, metamake_path),
			strequal(buf, "metadata-A") ? "/metadata-B" : "/metadata-A");
		
		dpvs(new_metadata_path);
		
		error = exec((char*[]) {ABS_GIT_PATH, "-C", new_metadata_path, "pull", NULL});
	}
	
	if (!error)
		error = ssymlink(new_metadata_path, ".tmp-link");
	
	if (!error)
		error = srename(".tmp-link", metadata_path);
	
	EXIT;
	return error;
}





















