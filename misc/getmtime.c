
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <debug.h>
#include <error.h>

#include <safe/sstat.h>

#include "getmtime.h"

int getmtime(time_t* outgoing_mtime, const char* full_path)
{
	int error = 0;
	struct stat statbuf;
	ENTER;
	
	dpvs(full_path);
	
	error = sstat(full_path, &statbuf);
	
	if (!error)
		*outgoing_mtime = statbuf.st_mtime;
	
	
	EXIT;
	return error;
}

