
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "defines/which/tar.h"
#include "defines/which/xz.h"
#include "defines/which/gunzip.h"
#include "defines/which/lzip.h"
#include "defines/which/curl.h"

#include <string/strendswith.h>

#include <safe/spipe2.h>

#include <execs/exec_wpiped_stdin.h>
#include <execs/exec_wpiped_stdout.h>

#include "stream_extract.h"

int stream_extract(
	char* url,
	char* extract_into)
{
	int error = 0;
	ENTER;
	
	dpvs(extract_into);
	dpvs(url);
	
	assert(extract_into);
	
	char* tar_command[50], **arg = tar_command;
	
	*arg++ = ABS_TAR_PATH;
	*arg++ = "-x";
	*arg++ = "-f", *arg++ = "-";
	*arg++ = "-C", *arg++ = extract_into;
	*arg++ = "--strip-components=1";
	
	if (strendswith(url, ".tar"))
		;
	else if (strendswith(url, ".tar.xz"))
		*arg++ = "-I", *arg++ = ABS_XZ_PATH;
	else if (strendswith(url, ".tar.gz"))
		*arg++ = "-I", *arg++ = ABS_GUNZIP_PATH;
	else if (strendswith(url, ".tar.lz"))
		*arg++ = "-I", *arg++ = ABS_LZIP_PATH;
	else
		fprintf(stderr, "%s: I don't know how to extract '%s'\n", argv0, url),
		error = e_unknown_tar_type;
	
	*arg++ = NULL;
	
	int pfd[2] = {-1, -1};
	
	if (!error)
		error = spipe2(pfd, O_CLOEXEC);
	
	pid_t curl_pid = -1, tar_pid = -1;
	int wstatus;
	
	if (!error)
		error = exec_wpiped_stdout(&curl_pid, pfd[1], (char* const[])
				{ABS_CURL_PATH, "-L", "--progress-bar", url, NULL});
	
	if (!error)
		error = exec_wpiped_stdin(&tar_pid, pfd[0], tar_command);
	
	if (pfd[1] > 0)
		close(pfd[1]);
	
	if (pfd[0] > 0)
		close(pfd[0]);
	
	if (curl_pid > 0)
	{
		if (waitpid(curl_pid, &wstatus, 0) < 0)
			perror("waitpid"),
			error = e_syscall_failed;
		else if (wstatus)
			error = e_subcommand_failed;
	}
	
	if (tar_pid > 0)
	{
		if (waitpid(tar_pid, &wstatus, 0) < 0)
			perror("waitpid"),
			error = e_syscall_failed;
		else if (wstatus)
			error = e_subcommand_failed;
	}
	
	EXIT;
	return error;
}


















