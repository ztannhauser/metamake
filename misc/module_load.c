
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#include <libexplain/waitpid.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include <execs/exec_wpiped_stdout.h>

#include <string_list/struct.h>

#include <safe/sgetenv.h>
#include <safe/spipe2.h>
#include <safe/ssetenv.h>
#include <safe/sfdopen.h>

#include "module_load.h"

int module_load(struct string_list* modules)
{
	int error = 0;
	size_t i, n;
	ENTER;
	
	assert(modules);
	
	char* modulecmd = NULL;
	error = sgetenv(&modulecmd, "MODULES_CMD");
	
	int pfd[2] = {-1, -1};
	pid_t tclsh_pid = -1;
	
	if (!error)
		error = spipe2(pfd, O_CLOEXEC);
	
	// $ tclsh /home/zander/libexec/modulecmd.tcl ruby load ffmpeg
	if (!error)
	{
		char* command[100], **arg = command;
		
		*arg++ = modulecmd, *arg++ = "ruby", *arg++ = "load";
		
		for (i = 0, n = modules->n; i < n; i++)
			*arg++ = modules->data[i]; 
		
		*arg++ = NULL;
		
		error = exec_wpiped_stdout(&tclsh_pid, pfd[1], command);
	}
	
	FILE* tclsh = NULL;
	
	if (pfd[1] > 0)
	{
		error = sfdopen(&tclsh, pfd[0], "r");
		close(pfd[1]), pfd[0] = -1;
	}
	
	struct {
		char* name, *value;
	} env = {NULL, NULL};
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;

	while (!error && (nread = getline(&line, &len, tclsh)) != -1)
	{
		env.name = realloc(env.name, nread);
		env.value = realloc(env.value, nread);
		
		if (sscanf(line, "ENV['%[^']'] = '%[^']'\n", env.name, env.value) == 2)
			error = ssetenv(env.name, env.value, true);
		else if (!strcmp(line, "_mlstatus = true\n"))
			error = 0;
		else if (!strcmp(line, "_mlstatus = false\n"))
			error = e_module_load_failed;
	}
	
	free(env.name), free(env.value);
	free(line);
	
	if (tclsh_pid > 0)
	{
		int wstatus;
		
		if (waitpid(tclsh_pid, &wstatus, 0) < 0)
			fprintf(stderr, "%s\n", explain_waitpid(tclsh_pid, &wstatus, 0)),
			error = e_syscall_failed;
		else if (!error && wstatus)
			fprintf(stderr, "%s: '%s' returned with non-zero exit code!\n", argv0, "tclsh"),
			error = e_subcommand_failed;
	}
	
	if (tclsh)
		fclose(tclsh);
	else if (pfd[0] > 0)
		close(pfd[0]);
	
	EXIT;
	return error;
}













