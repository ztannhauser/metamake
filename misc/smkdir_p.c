
#include <linux/limits.h>
#include <string.h>
#include <stdio.h>

#include <debug.h>

#include <safe/smkdir.h>

#include "smkdir_p.h"

int smkdir_p(const char* root, const char* _dirnames)
{
	int error = 0;
	ENTER;
	
	char path[PATH_MAX], *e;
	char dirnames[PATH_MAX], *moving = dirnames, *dirname;
	
	e = stpcpy(path, root);
	strcpy(dirnames, _dirnames);
	
	while (!error && (dirname = strtok_r(NULL, "/", &moving)))
	{
		dpvs(dirname);
		
		e = stpcpy(stpcpy(e, "/"), dirname);
		
		dpvs(path);
		
		error = smkdir(path, 0774);
	}
	
	EXIT;
	return error;
}

