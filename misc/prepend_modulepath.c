
#include <linux/limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include <debug.h>
#include <error.h>

#include <macros/N.h>

#include <defines/argv0.h>

#include <safe/smalloc.h>
#include <safe/ssetenv.h>

#include <safe/sgetenv.h>

#include "prepend_modulepath.h"

char message[PATH_MAX + 200];

int prepend_modulepath(const char* prependme_path, char** outgoing_message)
{
	int error = 0;
	ENTER;
	
	dpvs(prependme_path);
	
	char* oldmodulepath;
	error = sgetenv(&oldmodulepath, "MODULEPATH");
	
	if (error == e_bad_environment)
		fprintf(stderr, "%s: is the 'environment-modules' package installed?\n", argv0);
	
	char* newmodulepath = NULL;
	
	if (!error)
		error = smalloc((void**) &newmodulepath,
			strlen(prependme_path) + 1 + strlen(oldmodulepath) + 1);
	
	// search and possibly print out message:
	if (!error)
	{
		bool found = false;
		char* moving = newmodulepath, *path;
		
		strcpy(newmodulepath, oldmodulepath);
		
		while (!found && (path = strtok_r(NULL, ":", &moving)))
			if (!strcmp(path, prependme_path))
				found = true;
		
		if (!found)
		{
			snprintf(message, N(message),
				"%s: remember to run: `module use --prepend '%s'`\n"
				"%s: before attempting to run `module load`",
				argv0, prependme_path, argv0),
			
			*outgoing_message = message;
		}
	}
	
	// actually prepend path:
	if (!error)
	{
		strcat(strcat(strcpy(newmodulepath, prependme_path), ":"), oldmodulepath);
		
		dpvs(newmodulepath);
		
		error = ssetenv("MODULEPATH", newmodulepath, true);
	}
	
	free(newmodulepath);
	
	EXIT;
	return error;
}













