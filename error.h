
enum
{
	e_success,
	e_syscall_failed,
	e_bad_cmdline_args,
	e_out_of_memory,
	e_bad_environment,
	e_libcurl_init_failed,
	e_libcurl_download_failed,
	e_subcommand_failed,
	e_unknown_tar_type,
	e_invalid_utf8,
	e_json_syntax_error,
	e_bad_package_file,
	e_no_such_package,
	e_database_error,
	e_invalid_status_file,
	e_bad_package_param,
	e_invalid_stage,
	e_module_load_failed,
};

