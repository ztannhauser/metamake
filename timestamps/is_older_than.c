
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <debug.h>
#include <error.h>

#include <libexplain/access.h>

#include <misc/getmtime.h>

#include "is_older_than.h"

int is_older_than(bool* retval, const char* target, ...)
{
	int error = 0;
	va_list ap;
	ENTER;
	
	va_start(ap, target);
	
	bool is_older = false;
	
	// target might not exist:
	if (access(target, F_OK))
	{
		if (errno == ENOENT)
			is_older = true;
		else
			fprintf(stderr, "%s\n", explain_access(target, F_OK)),
			error = e_syscall_failed;
	}
	
	time_t target_mtime;
	if (!error && !is_older)
		error = getmtime(&target_mtime, target);
	
	const char* prerequisite;
	time_t prerequisite_mtime;
	
	while (!error && !is_older && (prerequisite = va_arg(ap, const char*)))
	{
		dpvs(prerequisite);
		
		error = getmtime(&prerequisite_mtime, prerequisite);
		
		dpv(target_mtime);
		dpv(prerequisite_mtime);
		
		if (!error && target_mtime < prerequisite_mtime)
			is_older = true;
	}
	
	if (!error)
	{
		dpvb(is_older);
		*retval = is_older;
	}
	
	va_end(ap);
	
	EXIT;
	return error;
}












