
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <safe/sopen.h>

#include "touch.h"

int touch(const char* path)
{
	int error = 0;
	ENTER;
	
	dpvs(path);
	
	int fd = -1;
	
	error = sopen(&fd, path, O_WRONLY | O_CREAT | O_NOCTTY | O_NONBLOCK, 0666);
	
	if (futimens(fd, (const struct timespec[2]) {
		{0, UTIME_NOW}, {0, UTIME_NOW}}) < 0)
	{
		perror("futimens"),
		error = e_syscall_failed;
	}
	
	if (fd > 0)
		close(fd);
	
	EXIT;
	return error;
}

