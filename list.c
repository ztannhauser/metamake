#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <ftw.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

/*#include <macros/N.h>*/

#include <safe/srealloc.h>
#include <safe/sstrndup.h>

#include <string/strendswith.h>

#include "list.h"

static int read_description(char** outgoing_description, const char* path)
{
	int error = 0;
	ENTER;
	
	int fd = -1;
	struct stat statbuf;
	char* data = NULL;
	
	if ((fd = open(path, O_RDONLY)) < 0)
		perror("open"),
		error = e_syscall_failed;
	else if (fstat(fd, &statbuf) < 0)
		perror("fstat"),
		error = e_syscall_failed;
	else if (!(data = malloc(statbuf.st_size + 1)))
		perror("malloc"),
		error = e_out_of_memory;
	else if (read(fd, data, statbuf.st_size) < statbuf.st_size)
		perror("read"),
		error = e_syscall_failed;
	else
	{
		// append null terminator:
		data[statbuf.st_size] = '\0';
		
		// remove newlines:
		int i;
		for (i = 0; data[i]; i++)
			if (data[i] == '\n')
				data[i] = ' ';
		
		dpvs(data);
		
		*outgoing_description = data, data = NULL;
	}
	
	if (fd > 0)
		close(fd);
	
	free(data);
	
	EXIT;
	return error;
}

static struct {
	struct entry {
		char* path;
		char* description;
	}* data;
	size_t n, cap;
} package_list = {NULL, 0, 0};

static int callback(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
	int error = 0;
	ENTER;
	
	if (strendswith(fpath, "/description.txt"))
	{
		dpvs(fpath);
		
		char* path = NULL;
		char* description = NULL;
		
		fpath += 2;
		
		error = sstrndup(&path, fpath, rindex(fpath, '/') - fpath);
		
		if (!error)
			error = read_description(&description, fpath);
		
		while (!error && package_list.n + 1 >= package_list.cap)
		{
			if (package_list.cap > 0)
				package_list.cap *= 2;
			else
				package_list.cap = 1;
			
			error = srealloc((void**) &package_list.data, sizeof(*package_list.data) * package_list.cap);
		}
		
		if (!error)
		{
			package_list.data[package_list.n++] = (struct entry) {path, description};
			
			path = NULL, description = NULL;
		}
		
		free(path), free(description);
	}
	
	EXIT;
	return error ? FTW_STOP : FTW_CONTINUE;
}

static int compar(const void* a, const void* b)
{
	const struct entry* A = a, *B = b;
	return strverscmp(A->path, B->path);
}

int list(const char* metadata_path)
{
	int error = 0;
	ENTER;
	
	if (chdir(metadata_path) < 0)
		perror("chdir"),
		error = e_syscall_failed;
	else if (nftw(".", callback, 100, FTW_ACTIONRETVAL) < 0)
		error = e_syscall_failed;
	
	size_t i, n;
	if (!error)
	{
		struct winsize w;
		int term_width = 80;
		
		char* path = NULL;
		char* description = NULL;
		
		qsort(package_list.data, package_list.n, sizeof(*package_list.data), compar);
		
		if (ioctl(0, TIOCGWINSZ, &w) >= 0)
			term_width = w.ws_col;
			
		int max_width = 0, ele_width;
		for (i = 0, n = package_list.n; i < n; i++)
		{
			ele_width = strlen(package_list.data[i].path);
			
			if (max_width < ele_width)
				max_width = ele_width;
		}
		
		for (i = 0, n = package_list.n; i < n; i++)
		{
			path = package_list.data[i].path;
			description = package_list.data[i].description;
			if (!description) description = "";
			
			ele_width = max_width + 1 + 1 + strlen(description) + 1 + 1;
			
			if (ele_width > term_width)
				printf("%-*s \"%.*s\"...\n", max_width, path, (int) (term_width - 7 - max_width), description);
			else
				printf("%-*s \"%s\"\n", max_width, path, description);
			
			free(path), free(description);
		}
	}
	
	if (!error)
	{
		if (package_list.n == 1)
			printf("1 package total.\n");
		else
			printf("%lu packages total.\n", package_list.n);
	}
	
	free(package_list.data);
	
	EXIT;
	return error;
}
















