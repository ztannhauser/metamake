
#include <stdio.h>
#include <linux/limits.h>
#include <unistd.h>

#include <debug.h>

#include <defines/argv0.h>

#include "cmdline/flags.h"
#include "cmdline/read_flags.h"
#include "cmdline/delete_flags.h"

#include "dirnames/struct.h"
#include "dirnames/init.h"
#include "dirnames/uninit.h"

#include "safe/ssetenv.h"

#include "misc/prepend_modulepath.h"

#include "install/main.h"

#include "update.h"
#include "list.h"

#if DEBUGGING
int debug_depth;
#endif

int main(int argc, char* const* argv)
{
	int error = 0;
	ENTER;
	
	struct cmdline_flags* flags = NULL;
	error = read_flags(&flags, argc, argv);
	
	struct dirnames* dirnames = NULL;
	if (!error)
		error = init_dirnames(&dirnames);
	
	if (!error)
		error = ssetenv("PACKAGES", dirnames->packages, true);
	
	char* modulepath_message = NULL;
	if (!error)
		error = prepend_modulepath(dirnames->modulefiles, &modulepath_message);
	
	char* arg;
	if (!error)
	{
		switch (flags->action)
		{
			case a_update:
				error = update(dirnames->metamake, dirnames->metadata);
				break;
			
			case a_list:
				error = list(dirnames->metadata);
				break;
			
/*			time_t now, then;*/
			case a_install:
				argv += optind;
				while (!error && (arg = *argv++))
				{
/*					now = time(NULL);*/
					
					error = install_main(dirnames, arg);
					
/*					if (!error && then < now)*/
/*						printf("%s: nothing to be done for '%s'\n", argv0, arg);*/
				}
				break;
		}
	}
	
	if (!error && modulepath_message)
		puts(modulepath_message);
	
	uninit_dirnames(dirnames);
	delete_flags(flags);
	
	if (error)
		printf("exit(%i);\n", error);
	
	EXIT;
	return error;
}






