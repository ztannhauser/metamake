
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <safe/saccess.h>

/*#include <macros/N.h>*/

#include <defines/argv0.h>

#include <dirnames/struct.h>

/*#include <misc/openmkdirat.h>*/
/*#include <misc/free_string_list.h>*/

#include <safe/ssetenv.h>

/*#include <string/strendswith.h>*/

/*#include <execs/exec.h>*/

#include <string_list/struct.h>
#include <string_list/delete.h>

#include <misc/smkdir_p.h>

#include "read_string_list.h"
#include "download.h"
#include "patch.h"
#include "autoconf.h"
#include "configure.h"
#include "make.h"
#include "install.h"
#include "write_modulefile.h"

#include "main.h"

int install_main(
	struct dirnames* dirnames,
	const char* package_name)
{
	int error = 0;
	ENTER;
	
	char package_metadata[PATH_MAX];
	char package_package[PATH_MAX];
	
	stpcpy(stpcpy(stpcpy(package_metadata, dirnames->metadata), "/"), package_name);
	stpcpy(stpcpy(stpcpy(package_package, dirnames->packages), "/"), package_name);
	
	dpvs(package_metadata);
	dpvs(package_package);
	
	error = saccess(package_metadata, R_OK | X_OK);
	
	if (!error)
		error = smkdir_p(dirnames->packages, package_name);
	
	struct string_list* buildtime = NULL;
	struct string_list* runtime = NULL;
	
	if (!error)
		error = read_string_list(&buildtime, package_metadata, "buildtime-deps.txt");
	
	if (!error)
		error = read_string_list(&runtime, package_metadata, "runtime-deps.txt");
	
	size_t i;
	for (i = 0; !error && i < buildtime->n; i++)
		error = install_main(dirnames, buildtime->data[i]);
	
	for (i = 0; !error && i < runtime->n; i++)
		error = install_main(dirnames, runtime->data[i]);
	
	char prefix[PATH_MAX];
	strcat(strcpy(prefix, package_package), "/prefix");
	
	dpvs(prefix);
	
	if (!error)
		error = 0
			?: ssetenv("PREFIX", prefix, true)
			?: download(package_name,
				package_metadata, package_package)
			?: patch(package_name, 
				package_metadata, package_package,
				buildtime)
			?: autoconf(package_name, 
				package_metadata, package_package,
				buildtime)
			?: configure(package_name, 
				package_metadata, package_package,
				buildtime)
			?: make(package_name, 
				package_metadata, package_package,
				buildtime)
			?: install(package_name, 
				package_metadata, package_package,
				buildtime)
			?: write_modulefile(package_name,
				package_metadata, package_package,
				dirnames->modulefiles,
				runtime);
	
	delete_string_list(runtime);
	delete_string_list(buildtime);
	
	if (error)
		printf("%s: ... when attempting to install %s\n", argv0, package_name);
	
	EXIT;
	return error;
}














