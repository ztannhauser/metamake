
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include <libexplain/access.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

/*#include <misc/getmtimeat.h>*/

#include <execs/exec_envml.h>

#include "patch.h"

int patch(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/patched");
	
	char download_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(download_timestamp_path, package_package), "/downloaded");
	
	char patch_script_path[PATH_MAX];
	stpcpy(stpcpy(patch_script_path, package_metadata), "/patch.sh");
	
	bool exists;
	
	if (!access(patch_script_path, F_OK))
		exists = true;
	else if (errno == ENOENT)
		exists = false;
	else
		fprintf(stderr, "%s\n", explain_access(patch_script_path, F_OK)),
		error = e_syscall_failed;
	
	bool older_than = false;
	if (!error)
		error = is_older_than(&older_than, timestamp_path,
			download_timestamp_path,
			exists ? patch_script_path : NULL,
			NULL);
	
	if (!error && older_than)
	{
		char src[PATH_MAX];
		strcat(strcpy(src, package_package), "/src");
		
		// execute patch script:
		if (exists)
			error = exec_envml(src, modules, (char* []){patch_script_path, NULL});
		
		// set new timestamp
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
}















