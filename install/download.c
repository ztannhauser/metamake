
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

#include <debug.h>
#include <error.h>

/*#include <macros/N.h>*/

#include <defines/argv0.h>

/*#include <libgdbm/check_modified.h>*/
/*#include <libgdbm/reset_timestamps.h>*/
/*#include <libgdbm/set_timestamp.h>*/

/*#include <misc/getmtimeat.h>*/

/*#include <json/get_string.h>*/

#include "read_string.h"

/*#include <string/strendswith.h>*/

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

#include <execs/exec.h>

#include <safe/smkdir.h>

#include <misc/stream_extract.h>

#include "download.h"

int download(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX])
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/downloaded");
	
	char url_file_path[PATH_MAX];
	stpcpy(stpcpy(url_file_path, package_metadata), "/url.txt");
	
	bool older_than = false;
	error = is_older_than(&older_than, timestamp_path,
		url_file_path,
		NULL);
	
	if (!error && older_than)
	{
		// read new URL
		char* url = NULL;
		if (!error)
			error = read_string(&url, url_file_path);
		
		dpvs(url);
		
		char src[PATH_MAX];
		strcat(strcpy(src, package_package), "/src");
		
		// delete old source
		if (!error)
			error = exec((char*[]){"rm", "-rf", src, NULL});
		
		// mkdir src
		if (!error)
			error = smkdir(src, 0775);
		
		// download/extract new source
		if (!error)
		{
			printf("%s: downloading source for '%s'...\n", argv0, package_name);
			error = stream_extract(url, src);
		}
		
		// set new timestamp
		if (!error)
			error = touch(timestamp_path);
		
		free(url);
	}
	
	EXIT;
	return error;
}


















