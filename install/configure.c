
#include <stdio.h>
#include <linux/limits.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

/*#include <misc/getmtimeat.h>*/

/*#include <libgdbm/reset_timestamps.h>*/
/*#include <libgdbm/check_modified.h>*/
/*#include <libgdbm/set_timestamp.h>*/

#include <safe/smkdir.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

/*#include <misc/readpathfd.h>*/

#include <libexplain/access.h>

#include <execs/exec.h>
#include <execs/exec_envml.h>
/*#include <execs/execat_envml.h>*/

#include "configure.h"

int configure(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/configured");
	
	char last_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(last_timestamp_path, package_package), "/autoconfigured");
	
	char configure_path[PATH_MAX];
	stpcpy(stpcpy(configure_path, package_metadata), "/configure.sh");
	
	bool exists;
	
	if (!access(configure_path, F_OK))
		exists = true;
	else if (errno == ENOENT)
		exists = false;
	else
		fprintf(stderr, "%s\n", explain_access(configure_path, F_OK)),
		error = e_syscall_failed;
	
	bool older_than = false;
	if (!error)
		error = is_older_than(&older_than, timestamp_path,
			last_timestamp_path,
			exists ? configure_path : NULL,
			NULL);
	
	if (!error && older_than)
	{
		char build[PATH_MAX];
		strcat(strcpy(build, package_package), "/build");
		
		// execute configure script:
		error = exec((char*[]){"rm", "-rf", build, NULL});
		
		if (!error)
			error = smkdir(build, 0775);
		
		if (!error && exists)
			error = exec_envml(build, modules, (char*[]) {configure_path, NULL});
		
		// set new timestamp
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
}















