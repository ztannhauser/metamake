
struct string_list;

int write_modulefile(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	char modulefiles_path[PATH_MAX],
	struct string_list* runtime_deps);
