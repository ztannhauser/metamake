
#include <linux/limits.h>

int download(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX]);
