
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <execs/exec.h>
#include <execs/exec_envml.h>

#include <defines/argv0.h>

/*#include <misc/getmtimeat.h>*/

#include <safe/smkdir.h>

#include <libexplain/access.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

/*#include <execs/execat_envml.h>*/

#include "make.h"

int make(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/made");
	
	char last_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(last_timestamp_path, package_package), "/configured");
	
	char make_script_path[PATH_MAX];
	stpcpy(stpcpy(make_script_path, package_metadata), "/make.sh");
	
	bool exists;
	
	if (!access(make_script_path, F_OK))
		exists = true;
	else if (errno == ENOENT)
		exists = false;
	else
		fprintf(stderr, "%s\n", explain_access(make_script_path, F_OK)),
		error = e_syscall_failed;
	
	bool older_than = false;
	if (!error)
		error = is_older_than(&older_than, timestamp_path,
			last_timestamp_path,
			exists ? make_script_path : NULL,
			NULL);
	
	if (!error && older_than)
	{
		char build[PATH_MAX];
		strcat(strcpy(build, package_package), "/build");
		
		error = smkdir(build, 0775);
		
		if (!error && exists)
			error = exec_envml(build, modules, (char*[]){make_script_path, NULL});
		
		// set new timestamp
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
	
}













