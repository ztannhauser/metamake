
#include <linux/limits.h>

struct string_list;

int patch(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules);
