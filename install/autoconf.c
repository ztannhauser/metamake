
#include <stdio.h>
#include <linux/limits.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

/*#include <misc/getmtimeat.h>*/

#include <libexplain/access.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

/*#include <execs/exec.h>*/
#include <execs/exec_envml.h>
/*#include <execs/execat_envml.h>*/

#include "autoconf.h"

int autoconf(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/autoconfigured");
	
	char last_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(last_timestamp_path, package_package), "/patched");
	
	char autoconf_script_path[PATH_MAX];
	stpcpy(stpcpy(autoconf_script_path, package_metadata), "/autoconf.sh");
	
	bool exists;
	
	if (!access(autoconf_script_path, F_OK))
		exists = true;
	else if (errno == ENOENT)
		exists = false;
	else
		fprintf(stderr, "%s\n", explain_access(autoconf_script_path, F_OK)),
		error = e_syscall_failed;
	
	bool older_than = false;
	if (!error)
		error = is_older_than(&older_than, timestamp_path,
			last_timestamp_path,
			exists ? autoconf_script_path : NULL,
			NULL);
	
	if (!error && older_than)
	{
		char src[PATH_MAX];
		strcat(strcpy(src, package_package), "/src");
		
		if (exists)
			error = exec_envml(src, modules, (char*[]){autoconf_script_path, NULL});
		
		// set new timestamp
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
}






















