
struct string_list;

int read_string_list(
	struct string_list** outgoing,
	const char* path, const char* filename);
