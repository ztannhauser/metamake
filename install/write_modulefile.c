
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <debug.h>
#include <error.h>

#include <macros/N.h>

#include <defines/argv0.h>
#include "defines/which/m4.h"

#include <string/mybasename.h>
#include <string/mydirname.h>

#include <misc/smkdir_p.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

#include <string_list/struct.h>

#include <execs/exec.h>
#include <execs/exec_wpiped_stdout.h>

#include <safe/sopen.h>

#include "write_modulefile.h"

int write_modulefile(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	char modulefiles_path[PATH_MAX],
	struct string_list* runtime_deps)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/modulefile_written");
	
	char last_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(last_timestamp_path, package_package), "/installed");
	
	char modulefile_path[PATH_MAX];
	stpcpy(stpcpy(modulefile_path, package_metadata), "/modulefile.m4");
	
	bool older_than = false;
	error = is_older_than(&older_than, timestamp_path,
		last_timestamp_path,
		modulefile_path,
		NULL);
	
	if (!error && older_than)
	{
		char* dirname;
		
		dirname = mydirname(package_name);
		
		dpvs(dirname);
		
		error = smkdir_p(modulefiles_path, dirname);
		
		int modulefile_fd = -1;
		if (!error)
		{
			char modulefile_path[PATH_MAX];
			
			strcat(strcat(strcpy(modulefile_path, modulefiles_path), "/"), package_name);
			
			error = sopen(&modulefile_fd, modulefile_path, O_CREAT | O_WRONLY | O_TRUNC, 0664);
		}
		
		size_t i, n;
		
		if (!error)
		{
			dprintf(modulefile_fd, "#%%Module1.0#\n");
			
			for (i = 0, n = runtime_deps->n; i < n; i++)
				dprintf(modulefile_fd, "module load %s\n", runtime_deps->data[i]);
		}
		
		char prefix[PATH_MAX];
		strcat(strcat(strcpy(prefix, "$PREFIX="), package_package), "/prefix");
		
		pid_t m4_pid = -1;
		
		if (!error)
			error = exec_wpiped_stdout(
				&m4_pid,
				modulefile_fd,
				(char*[]) {
					ABS_M4_PATH,
					"-D", prefix,
					modulefile_path,
					NULL
				}
			);
		
		if (m4_pid > 0)
		{
			int wstatus;
			
			if (waitpid(m4_pid, &wstatus, 0) < 0)
				perror("waitpid"),
				error = e_syscall_failed;
			else if (wstatus)
				error = e_subcommand_failed;
		}
		
		if (modulefile_fd > 0)
			close(modulefile_fd);
		
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
}















