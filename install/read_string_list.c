
#include <linux/limits.h>
#include <string.h>
#include <stdlib.h>

#include <debug.h>

#include <macros/strequal.h>

#include <string_list/new.h>
#include <string_list/append.h>
#include <string_list/delete.h>

#include <safe/sfopen.h>
/*#include <safe/srealloc.h>*/
#include <safe/sstrdup.h>

#include "read_string_list.h"

int read_string_list(
	struct string_list** outgoing,
	const char* path, const char* filename)
{
	int error = 0;
	ENTER;
	
	dpvs(path); dpvs(filename);
	
	char full_path[PATH_MAX];
	
	stpcpy(stpcpy(stpcpy(full_path, path), "/"), filename);
	
	dpvs(full_path);
	
	FILE* stream = NULL;
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	
	struct string_list* list = NULL;
	
	error = new_string_list(&list);
	
	if (!error)
		error = sfopen(&stream, full_path, "r");
	
	while (!error && (nread = getline(&line, &len, stream)) != -1)
	{
		line[--nread] = '\0';
		
		if (!strequal(line, ""))
		{
			dpvs(line);
			
			char* duped = NULL;
			
			error = sstrdup(&duped, line);
			
			if (!error)
				error = string_list_append(list, duped);
			
			if (error)
				free(duped);
		}
	}
	
	if (!error)
		*outgoing = list, list = NULL;
	
	if (stream)
		fclose(stream);
	
	free(line);
	delete_string_list(list);
	
	EXIT;
	return error;
}











