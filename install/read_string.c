
#include <string.h>
#include <stdlib.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

/*#include <macros/strequal.h>*/

#include <safe/sfopen.h>
/*#include <safe/srealloc.h>*/
/*#include <safe/sstrdup.h>*/

#include "read_string.h"

int read_string(
	char** outgoing_string,
	const char* full_path)
{
	int error = 0;
	ENTER;
	
	dpvs(full_path);
	
	FILE* stream = NULL;
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	
	error = sfopen(&stream, full_path, "r");
	
	if (!error && (nread = getline(&line, &len, stream)) < 0)
		fprintf(stderr, "%s: getline(): %s\n", argv0, strerror(errno)),
		error = e_syscall_failed;
	
	if (!error)
	{
		line[--nread] = '\0';
		
		*outgoing_string = line, line = NULL;
	}
	
	if (stream)
		fclose(stream);
	
	free(line);
	
	EXIT;
	return error;
}











