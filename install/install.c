
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h> 
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

/*#include <misc/getmtimeat.h>*/

#include <safe/smkdir.h>

#include <timestamps/is_older_than.h>
#include <timestamps/touch.h>

#include <execs/exec_envml.h>

#include "install.h"

int install(
	const char* package_name,
	char package_metadata[PATH_MAX],
	char package_package[PATH_MAX],
	struct string_list* modules)
{
	int error = 0;
	ENTER;
	
	char timestamp_path[PATH_MAX];
	stpcpy(stpcpy(timestamp_path, package_package), "/installed");
	
	char last_timestamp_path[PATH_MAX];
	stpcpy(stpcpy(last_timestamp_path, package_package), "/made");
	
	char install_script_path[PATH_MAX];
	stpcpy(stpcpy(install_script_path, package_metadata), "/install.sh");
	
	bool older_than = false;
	error = is_older_than(&older_than, timestamp_path,
		last_timestamp_path,
		install_script_path,
		NULL);
	
	if (!error && older_than)
	{
		char build[PATH_MAX];
		strcat(strcpy(build, package_package), "/build");
		
		error = smkdir(build, 0775);
		
		if (!error)
			error = exec_envml(build, modules, (char*[]){install_script_path, NULL});
			
		if (!error)
			error = touch(timestamp_path);
	}
	
	EXIT;
	return error;
}




























