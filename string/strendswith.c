
#include <string.h>
#include <stddef.h>

#include "strendswith.h"

bool strendswith(const char* string, const char* suffix)
{
	size_t a = strlen(string), b = strlen(suffix);
	return a >= b && !strcmp(string + a - b, suffix);
}

