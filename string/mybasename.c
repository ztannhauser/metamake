
#include <stdio.h>
#include <string.h>

#include <debug.h>

#include "mybasename.h"

static const char* myindex(const char* string, char c)
{
	const char* retval = NULL;
	const char* start = string, *end = start + strlen(string);
	
	for (const char* i = start; !retval && i < end; i++)
		if (*i == c)
			retval = i;
	
	return retval;
}

const char* mybasename(const char* path)
{
	const char* slash, *slash2;
	const char* retval;
	ENTER;
	
	slash = myindex(path, '/');
	
	if (!slash)
		retval = path;
	else
	{
		while (slash2 = slash, slash[1] && (slash = myindex(slash + 1, '/')));
		
		dpvs(slash2 + 1);
		
		retval = slash2 + 1;
	}
	
	EXIT;
	return retval;
}














