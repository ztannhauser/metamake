
#include <linux/limits.h>
#include <string.h>

#include <debug.h>

#include "mydirname.h"

static const char* myrindex(const char* string, char c)
{
	const char* retval = NULL;
	const char* start = string, *end = start + strlen(string);
	
	for (const char* i = end - 1; !retval && i >= start; i--)
		if (*i == c)
			retval = i;
	
	return retval;
}

static char returnme[PATH_MAX];

char* mydirname(const char* path)
{
	const char* slash;
/*	const char* retval;*/
	ENTER;
	
	if (!(slash = myrindex(path, '/')))
		strcpy(returnme, ".");
	else if (slash == path)
		strcpy(returnme, "/");
	else
	{	
		stpncpy(returnme, path, slash - path)[0] = '\0';
		
		dpvs(returnme);
	}
	
	EXIT;
	return returnme;
}














