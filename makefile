#!/usr/bin/make -f

SHELL = /bin/bash

buildtype = release

CC = gcc

CPPFLAGS += -I . 
CPPFLAGS += -D _GNU_SOURCE

CFLAGS += -Wall -Werror

ifeq ($(buildtype), release)
CPPFLAGS += -D DEBUGGING=0
CFLAGS += -O2
CFLAGS += -flto
LDFLAGS += -static
else
CPPFLAGS += -D DEBUGGING=1
CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

LDLIBS += -lexplain
LDLIBS += -lcap
LDLIBS += -ldl

#ARGS += -v
ARGS += --update
#ARGS += --list
#ARGS += --install ffmpeg/4.4
#ARGS += --install m4/1.4.18
#ARGS += --install bison/3.7.6

default: bin/$(buildtype)/metamake

run: bin/$(buildtype)/metamake
	$< $(ARGS)

valrun: bin/$(buildtype)/metamake
	valgrind --gen-suppressions=yes $< $(ARGS)

valrun-leak: bin/$(buildtype)/metamake
	valgrind --leak-check=full --show-leak-kinds=all $< $(ARGS)

install: ~/bin/metamake

~/bin/metamake: bin/release/metamake
	install $< -D $@

bin/:
	for f in $$(find * -type d ! -path 'external/*'); \
	do mkdir -p bin/debug/$$f bin/release/$$f; done

bin/srclist.mk: | bin/
	find -name '*.c' ! -path './external/*' | sed 's/^/srcs += /' > $@

ifneq "$(MAKECMDGOALS)" "clean"
include bin/srclist.mk
endif

defines/which/:
	mkdir -p $@

defines/which/%.h: | defines/which/
	which $* | read
	x=$*; echo "#define ABS_$${x^^}_PATH \"`which $*`\"" > $@

objs = $(patsubst %.c,bin/$(buildtype)/%.o,$(srcs))
deps = $(patsubst %.c,bin/$(buildtype)/%.d,$(srcs))

bin/$(buildtype)/metamake: $(objs)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/$(buildtype)/%.d: %.c
	$(CPP) $(CPPFLAGS) $< -MM -MG -MT $@ -MF $@

bin/$(buildtype)/%.o: %.c bin/$(buildtype)/%.d
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

clean:
	for l in $$(cat .gitignore); do rm -rvf $$l; done

ifneq "$(MAKECMDGOALS)" "clean"
include $(deps)
endif

















