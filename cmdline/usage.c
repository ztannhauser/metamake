
#include <errno.h>
#include <stdio.h>

#include <debug.h>

#include <defines/argv0.h>

#include "usage.h"

void usage()
{
	printf("usage examples:\n");
	printf("$ %s --update\n", argv0);
	printf("$ %s --list\n", argv0);
	printf("$ %s --install gcc@10.2.0\n", argv0);
}

