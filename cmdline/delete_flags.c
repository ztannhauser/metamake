
#include <stdlib.h>

#include <debug.h>

#include "delete_flags.h"

void delete_flags(struct cmdline_flags* flags)
{
	ENTER;
	
	if (flags)
	{
		free(flags);
	}
	
	EXIT;
}

