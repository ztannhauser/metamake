
#include <stdlib.h>
#include <getopt.h>

#include <error.h>
#include <debug.h>

#include <safe/smalloc.h>

#include "flags.h"
#include "usage.h"
#include "read_flags.h"

int read_flags(struct cmdline_flags** retval, int argc, char* const* argv)
{
	int error = 0;
	ENTER;
	
	enum cmdline_action action = 0;
	
	bool verbose = false;
	
	// for getopt:
	int opt, option_index;
	const struct option long_options[] = {
		{"update", no_argument, 0, 0},
		{"list", no_argument, 0, 0},
		{"install", no_argument, 0, 0},
		{0, 0, 0, 0}
	};
	
	while (!error && (opt = getopt_long(argc, argv, "ulivh", long_options, &option_index)) >= 0)
	{
		switch (opt ? opt : option_index)
		{
			case 'u':
			case 0:
				action = a_update;
				break;
			
			case 'l':
			case 1:
				action = a_list;
				break;
			
			case 'i':
			case 2:
				action = a_install;
				break;
			
			case 'v':
				verbose = true;
				break;
			
			case 'h':
				TODO;
				break;
			
			default:
				error = e_bad_cmdline_args;
				break;
		}
	}
	
	if (!error && !action)
		error = e_bad_cmdline_args;
	
	if (error == e_bad_cmdline_args)
	{
		usage();
	}
	
	struct cmdline_flags* new = NULL;
	
	if (!error)
		error = smalloc((void**) &new, sizeof(*new));
	
	if (!error)
	{
		new->action = action;
		new->verbose = verbose;
	}
	
	if (error)
		free(new);
	else
		*retval = new;
	
	EXIT;
	return error;
}
















