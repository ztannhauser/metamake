
#include <stdbool.h>

struct cmdline_flags
{
	enum cmdline_action {
		a_update = 1,
		a_list,
		a_install,
	} action;
	
	bool verbose;
};

