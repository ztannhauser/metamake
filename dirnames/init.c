
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>
#include <defines/REPO_URL.h>
#include "defines/which/git.h"

#include <execs/exec.h>

#include <safe/smalloc.h>
#include <safe/smkdir.h>
#include <safe/sgetenv.h>
#include <safe/ssymlink.h>

#include "struct.h"
#include "init.h"

#if DEBUGGING
#define SUFFIX ".debug"
#else
#define SUFFIX ""
#endif

int init_dirnames(struct dirnames** retval)
{
	int error = 0;
	ENTER;
	
	char* home_path;
	error = sgetenv(&home_path, "HOME");
	
	dpvs(home_path);
	
	char home[PATH_MAX];
	char metamake[PATH_MAX];
	char packages[PATH_MAX];
	char metadata[PATH_MAX];
	char modulefiles[PATH_MAX];
	
	if (!error)
	{
		strcpy(home, home_path);
		stpcpy(stpcpy(metamake, home), "/.metamake" SUFFIX);
		stpcpy(stpcpy(packages, metamake), "/packages");
		stpcpy(stpcpy(metadata, metamake), "/metadata");
		stpcpy(stpcpy(modulefiles, metamake), "/modulefiles");
		
		dpvs(home);
		dpvs(metamake);
		dpvs(packages);
		dpvs(metadata);
		dpvs(modulefiles);
	}
	
	if (!error)
		error = smkdir(metamake, 0774);
	
	if (!error)
		error = smkdir(packages, 0774);
	
	if (!error)
		error = smkdir(modulefiles, 0774);
	
	dpv(error);
	
	if (!error && access(metadata, R_OK | W_OK | X_OK) && errno == ENOENT)
	{
		printf("%s: performing first-time setup...\n", argv0);
		
		char metadata_A[PATH_MAX], metadata_B[PATH_MAX];
		
		stpcpy(stpcpy(metadata_A, metadata), "-A");
		stpcpy(stpcpy(metadata_B, metadata), "-B");
		
		error = exec((char*[]){ABS_GIT_PATH, "clone", REPO_URL, metadata_A, NULL});
		
		if (!error)
			error = exec((char*[]){ABS_GIT_PATH, "clone", REPO_URL, metadata_B, NULL});
		
		if (!error)
			error = ssymlink("metadata-A", metadata);
	}
	
	struct dirnames* dirnames = NULL;
	
	if (!error)
		error = smalloc((void**) &dirnames, sizeof(*dirnames));
	
	if (!error)
	{
		strcpy(dirnames->home, home);
		strcpy(dirnames->metamake, metamake);
		strcpy(dirnames->packages, packages);
		strcpy(dirnames->metadata, metadata);
		strcpy(dirnames->modulefiles, modulefiles);
		
		*retval = dirnames;
	}
	
	EXIT;
	return error;
}











