
#include <stdlib.h>

#include <debug.h>

#include "uninit.h"

void uninit_dirnames(struct dirnames* dirnames)
{
	ENTER;
	
	free(dirnames);
	
	EXIT;
}

