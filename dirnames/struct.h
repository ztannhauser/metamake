
#include <linux/limits.h>

struct dirnames
{
	char home[PATH_MAX];
	char metamake[PATH_MAX];
	char packages[PATH_MAX];
	char metadata[PATH_MAX];
	char modulefiles[PATH_MAX];
};

