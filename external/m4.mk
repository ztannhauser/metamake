
tars/m4.tar.xz: URL=https://ftp.gnu.org/gnu/m4/m4-1.4.19.tar.xz
tars/m4.tar.xz: | tars/
	wget -nc ${URL} -O $@

m4/configured: m4/downloaded | m4/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

m4/built: m4/configured
	make -C m4/build/ -j `nproc`
	touch $@

m4/installed: m4/built
	make -C m4/build/ install
	touch $@


