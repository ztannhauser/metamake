
tars/libgdbm.tar.gz: URL=https://ftp.gnu.org/gnu/gdbm/gdbm-1.19.tar.gz
tars/libgdbm.tar.gz: | tars/
	wget -nc ${URL} -O $@

libgdbm/configured: libgdbm/downloaded | libgdbm/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

libgdbm/built: libgdbm/configured
	make -C libgdbm/build/ -j `nproc`
	touch $@

libgdbm/installed: libgdbm/built
	make -C libgdbm/build/ install
	touch $@


