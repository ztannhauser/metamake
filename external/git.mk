
tars/git.tar.xz: URL=https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.25.1.tar.xz
tars/git.tar.xz: | tars/
	wget -nc ${URL} -O $@

git/configured: git/downloaded
	cd git/src; ../src/configure --prefix=${PREFIX}
	touch $@

git/built: git/configured
	make -C git/src/ -j `nproc`
	touch $@

git/installed: git/built
	make -C git/src/ install
	touch $@


