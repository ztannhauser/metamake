
tars/libcap.tar.xz: URL=https://mirrors.edge.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-2.48.tar.xz
tars/libcap.tar.xz: | tars/
	wget -nc ${URL} -O $@

libcap/built: libcap/downloaded
	make -C libcap/src/ -j `nproc`
	touch $@

libcap/installed: libcap/built
	make -C libcap/src/ install prefix=$(PREFIX) lib=lib
	touch $@


