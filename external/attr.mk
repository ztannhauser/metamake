
tars/attr.tar.xz: URL=http://download.savannah.nongnu.org/releases/attr/attr-2.5.1.tar.xz
tars/attr.tar.xz: | tars/
	wget -nc ${URL} -O $@

attr/configured: attr/downloaded | attr/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

attr/built: attr/configured
	make -C attr/build/ -j `nproc`
	touch $@

attr/installed: attr/built
	make -C attr/build/ install
	touch $@


