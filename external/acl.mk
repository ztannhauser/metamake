
tars/acl.tar.xz: URL=http://download.savannah.nongnu.org/releases/acl/acl-2.3.1.tar.xz
tars/acl.tar.xz: | tars/
	wget -nc ${URL} -O $@

acl/configured: acl/downloaded attr/installed | acl/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

acl/built: acl/configured
	make -C acl/build/ -j `nproc`
	touch $@

acl/installed: acl/built
	make -C acl/build/ install
	touch $@


