
tars/mpfr.tar.gz: URL=https://ftp.gnu.org/gnu/mpfr/mpfr-2.4.2.tar.gz
tars/mpfr.tar.gz: | tars/
	wget -nc ${URL} -O $@

mpfr/configured: mpfr/downloaded gmp/installed | mpfr/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

mpfr/built: mpfr/configured
	make -C mpfr/build/ -j `nproc`
	touch $@

mpfr/installed: mpfr/built
	make -C mpfr/build/ install
	touch $@


