
tars/libtool.tar.xz: URL=https://ftp.gnu.org/gnu/libtool/libtool-2.4.tar.xz
tars/libtool.tar.xz: | tars/
	wget -nc ${URL} -O $@

libtool/configured: libtool/downloaded | libtool/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

libtool/built: libtool/configured
	make -C libtool/build/ -j `nproc`
	touch $@

libtool/installed: libtool/built
	make -C libtool/build/ install
	touch $@


