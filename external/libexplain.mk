
tars/libexplain.tar.gz: URL=https://versaweb.dl.sourceforge.net/project/libexplain/1.4/libexplain-1.4.tar.gz
tars/libexplain.tar.gz: | tars/
	wget -nc ${URL} -O $@

libexplain/patched: libexplain/downloaded libexplain.patch
	test $$(hostname) = "thor.cs.wmich.edu" || patch -p1 < libexplain.patch
	touch $@

libexplain/configured: libexplain/patched \
	libtool/installed \
	bison/installed libcap/installed groff/installed acl/installed
	cd libexplain/src; ./configure --prefix=${PREFIX}
	touch $@

libexplain/built: libexplain/configured
	make -C libexplain/src/ -j `nproc`
	touch $@

libexplain/installed: libexplain/built
	make -C libexplain/src/ install
	touch $@


