
tars/bzip2.tar.gz: URL=https://www.sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz
tars/bzip2.tar.gz: | tars/
	wget -nc ${URL} -O $@

bzip2/built: bzip2/downloaded
	make -C bzip2/src clean
	make -C bzip2/src -f Makefile-libbz2_so
	make -C bzip2/src clean
	make -C bzip2/src
	touch $@

bzip2/installed: bzip2/built
	make -C bzip2/src PREFIX=${PREFIX} install
	cp -av ${PREFIX}/libbz2.so* ${PREFIX}/lib; \
	ln -sv libbz2.so.1.0 ${PREFIX}/lib/libbz2.so
	touch $@


