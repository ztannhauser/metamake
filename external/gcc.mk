
tars/gcc.tar.gz: URL=https://ftp.gnu.org/gnu/gcc/gcc-5.3.0/gcc-5.3.0.tar.gz
tars/gcc.tar.gz: | tars/
	wget -nc ${URL} -O $@

gcc/configured: gcc/downloaded \
	gmp/installed mpfr/installed mpc/installed | gcc/build/
	cd $|; ../src/configure --prefix=${PREFIX} \
		--disable-bootstrap \
		--enable-languages=c \
		--disable-multilib
	touch $@

gcc/built: gcc/configured
	make -C gcc/build/ -j `nproc`
	touch $@

gcc/installed: gcc/built
	make -C gcc/build/ install
	touch $@


