
tars/texinfo.tar.xz: URL=https://ftp.gnu.org/gnu/texinfo/texinfo-6.7.tar.xz
tars/texinfo.tar.xz: | tars/
	wget -nc ${URL} -O $@

texinfo/configured: texinfo/downloaded | texinfo/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

texinfo/built: texinfo/configured
	make -C texinfo/build/ -j `nproc`
	touch $@

texinfo/installed: texinfo/built
	make -C texinfo/build/ install
	touch $@


