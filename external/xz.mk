
tars/xz.tar.gz: URL=https://tukaani.org/xz/xz-5.2.5.tar.gz
tars/xz.tar.gz: | tars/
	wget -nc ${URL} -O $@

xz/configured: xz/downloaded | xz/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

xz/built: xz/configured
	make -C xz/build/ -j `nproc`
	touch $@

xz/installed: xz/built
	make -C xz/build/ install
	touch $@


