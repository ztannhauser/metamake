
tars/gmp.tar.gz: URL=https://ftp.gnu.org/gnu/gmp/gmp-4.3.2.tar.gz
tars/gmp.tar.gz: | tars/
	wget -nc ${URL} -O $@

gmp/configured: gmp/downloaded m4/installed | gmp/build/
	cd $|; ../src/configure --prefix=${PREFIX} \
		--disable-static
	touch $@

gmp/built: gmp/configured
	make -C gmp/build/ -j `nproc`
	touch $@

gmp/installed: gmp/built
	make -C gmp/build/ install
	touch $@


