
tars/mpc.tar.gz: URL=https://ftp.gnu.org/gnu/mpc/mpc-1.0.2.tar.gz
tars/mpc.tar.gz: | tars/
	wget -nc ${URL} -O $@

mpc/configured: mpc/downloaded mpfr/installed gmp/installed | mpc/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

mpc/built: mpc/configured
	make -C mpc/build/ -j `nproc`
	touch $@

mpc/installed: mpc/built
	make -C mpc/build/ install
	touch $@


