
tars/lzip.tar.gz: URL=http://download.savannah.gnu.org/releases/lzip/lzip-1.22.tar.gz
tars/lzip.tar.gz: | tars/
	wget -nc ${URL} -O $@

lzip/configured: lzip/downloaded | lzip/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

lzip/built: lzip/configured
	make -C lzip/build/ -j `nproc`
	touch $@

lzip/installed: lzip/built
	make -C lzip/build/ install
	touch $@


