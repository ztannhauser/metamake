#! /bin/bash
set -ev
cd external
mkdir -p make tars
if [ ! -f make/downloaded ]
then
	wget -nc https://ftp.gnu.org/gnu/make/make-4.3.tar.gz -O tars/make.tar.gz
	touch make/downloaded
fi
if [ ! -f make/extracted -o make/extracted -ot make/downloaded ]
then
	mkdir -p make/src
	tar -zxf tars/make.tar.gz -C make/src --strip-components=1
	touch make/extracted
fi
if [ ! -f make/configured -o make/configured -ot make/extracted ]
then
	mkdir -p make/build
	PREFIX=${PWD}
	(cd make/build; ../src/configure --prefix=${PREFIX})
	touch make/configured
fi
if [ ! -f make/built -o make/made -ot make/configured ]
then
	make -j `nproc` -C make/build
	touch make/built
fi
if [ ! -f make/installed -o make/installed -ot make/built ]
then
	make -C make/build install
	touch make/installed
fi
