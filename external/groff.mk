
tars/groff.tar.gz: URL=https://ftp.gnu.org/gnu/groff/groff-1.22.4.tar.gz
tars/groff.tar.gz: | tars/
	wget -nc ${URL} -O $@

groff/configured: groff/downloaded texinfo/installed | groff/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

groff/built: groff/configured
	make -C groff/build/ -j `nproc`
	touch $@

groff/installed: groff/built
	make -C groff/build/ install
	touch $@


