
#include <debug.h>

#include <safe/smalloc.h>

#include "struct.h"
#include "new.h"

int new_string_list(struct string_list** new)
{
	int error = 0;
	ENTER;
	
	struct string_list* this = NULL;
	
	error = smalloc((void**) &this, sizeof(*this));
	
	if (!error)
	{
		this->data = NULL;
		this->n = 0;
		this->cap = 0;
		*new = this;
	}
	
	EXIT;
	return error;
}

