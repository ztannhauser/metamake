
#include <stdlib.h>

#include <debug.h>

#include "struct.h"
#include "delete.h"

void delete_string_list(struct string_list* this)
{
	size_t i, n;
	ENTER;
	if (this)
	{
		for (i = 0, n = this->n; i < n; i++)
		{
			free(this->data[i]);
		}
		
		free(this->data);
		free(this);
	}
	EXIT;
}

