
#include <debug.h>

#include <safe/srealloc.h>

#include "struct.h"
#include "append.h"

int string_list_append(struct string_list* this, char* ele)
{
	int error = 0;
	ENTER;
	
	if (this->n + 1 >= this->cap)
	{
		if (this->cap)
			this->cap *= 2;
		else
			this->cap = 1;
		
		error = srealloc((void**) &this->data, sizeof(*this->data) * this->cap);
	}
	
	if (!error)
		this->data[this->n++] = ele;
	
	EXIT;
	return error;
}

